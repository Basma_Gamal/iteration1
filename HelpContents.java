import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTextPane;

public class HelpContents {

	private JFrame helpFrame;
	
	// to run as main to see it for now
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HelpContents window = new HelpContents();
					window.helpFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		

		
	}
	*/

	/**
	 * Create the application.
	 */
	public HelpContents() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		helpFrame = new JFrame();
		helpFrame.setBounds(100, 100, 450, 300);
		helpFrame.setTitle("Help - Gomoku");
		helpFrame.setVisible(true);

		JTextPane textPane = new JTextPane();
		textPane.setText(read());
		textPane.setContentType("text/plain");
		textPane.setEditable(false);
		helpFrame.getContentPane().add(textPane, BorderLayout.CENTER);

	}

	private String read() {
		File file = new File("H://Eclipse Java//eclipse//WorkSpace//Gomoku//rule.txt");

		String contents ="";

		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new FileReader(file));

			String text = "";

			// repeat until all lines is read

			while ((text = reader.readLine()) != null) {

				contents=contents + text +"\n";
			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (reader != null) {

					reader.close();

				}

			} catch (IOException e) {

				e.printStackTrace();

			}

		}
		return contents;

	}

}
